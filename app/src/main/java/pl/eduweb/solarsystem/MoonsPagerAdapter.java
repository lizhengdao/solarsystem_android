package pl.eduweb.solarsystem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class MoonsPagerAdapter extends FragmentStatePagerAdapter {

    private final SolarObject[] objectsWithMoons;

    public MoonsPagerAdapter(@NonNull FragmentManager fm, int behavior, SolarObject[] objectsWithMoons) {
        super(fm, behavior);
        this.objectsWithMoons = objectsWithMoons;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return SolarObjectsFragment.newInstance(objectsWithMoons[position].getMoons());
    }

    @Override
    public int getCount() {
        return objectsWithMoons.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return objectsWithMoons[position].getName();
    }
}
