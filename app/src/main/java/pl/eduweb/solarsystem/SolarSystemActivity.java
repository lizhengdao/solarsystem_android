package pl.eduweb.solarsystem;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SolarSystemActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, MoonsFragment.TabCallback {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.moonsTabLayout)
    TabLayout moonsTabLayout;
    @BindView(R.id.containerLayout)
    FrameLayout containerLayout;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    private AppBarConfiguration mAppBarConfiguration;
    private SolarObject[] planets;
    private SolarObject[] others;
    private SolarObject[] objectsWithMoons;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solar_system);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navView.setNavigationItemSelectedListener(this);

        planets = SolarObject.loadArrayFromJson(this, "planets");
        others = SolarObject.loadArrayFromJson(this, "others");

        ArrayList<SolarObject> arrayList = new ArrayList<>();

        for (SolarObject planet : planets) {
            if (planet.hasMoon()) {
                arrayList.add(planet);
            }
        }

        for (SolarObject other : others) {
            if (other.hasMoon()) {
                arrayList.add(other);
            }
        }

        objectsWithMoons = new SolarObject[arrayList.size()];

        objectsWithMoons = arrayList.toArray(objectsWithMoons);


        navView.setCheckedItem(R.id.nav_planets);
        onNavigationItemSelected(navView.getMenu().findItem(R.id.nav_planets));

//        DrawerLayout drawer = findViewById(R.id.drawer_layout);
//        NavigationView navigationView = findViewById(R.id.nav_view);
//        // Passing each menu ID as a set of Ids because each
//        // menu should be considered as top level destinations.
//        mAppBarConfiguration = new AppBarConfiguration.Builder(
//                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow)
//                .setDrawerLayout(drawer)
//                .build();
//        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
//        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
//        NavigationUI.setupWithNavController(navigationView, navController);
    }


//    @Override
//    public boolean onSupportNavigateUp() {
//        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
//        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
//                || super.onSupportNavigateUp();
//    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_planets) {
            SolarObjectsFragment fragment = SolarObjectsFragment.newInstance(planets);

            replaceFragment(fragment);


        } else if (id == R.id.nav_moons) {

            replaceFragment(MoonsFragment.newInstance(objectsWithMoons));

        } else if (id == R.id.nav_other) {

            SolarObjectsFragment fragment = SolarObjectsFragment.newInstance(others);

            replaceFragment(fragment);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById((R.id.drawer_layout));
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void replaceFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.containerLayout, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void showTabs(ViewPager viewPager) {
        moonsTabLayout.setVisibility(View.VISIBLE);
        moonsTabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void hideTabs() {
        moonsTabLayout.removeAllTabs();
        moonsTabLayout.setVisibility(View.GONE);
    }
}